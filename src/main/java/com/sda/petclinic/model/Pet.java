package com.sda.petclinic.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "pets")
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long petId;

    @Column(name = "race")
    private String race;

    @Column(name = "birthday")
    private Date birthdate;

    @Column(name = "is_vaccinated")
    private boolean isVaccinated;

    @Column(name = "owner_name")
    private String ownerName;

    @OneToMany(mappedBy = "pet")
    private List<Consult> petConsultList;

    public Pet() {
    }

    public Pet(String race, Date birthdate, boolean isVaccinated, String ownerName) {
        this.race = race;
        this.birthdate = birthdate;
        this.isVaccinated = isVaccinated;
        this.ownerName = ownerName;
    }

    public long getPetId() {
        return petId;
    }

    public void setPetId(long petId) {
        this.petId = petId;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isVaccinated() {
        return isVaccinated;
    }

    public void setIsvaccinated(boolean isvaccinated) {
        this.isVaccinated = isvaccinated;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Pet(String race, Date birthdate, boolean isvaccinated, String ownerName, List<Consult> petConsultList) {
        this.race = race;
        this.birthdate = birthdate;
        this.isVaccinated = isvaccinated;
        this.ownerName = ownerName;
        this.petConsultList = petConsultList;
    }

//    public List<Consult> getPetConsultList() {
//        return petConsultList;
//    }
//
//    public void setPetConsultList(List<Consult> petConsultList) {
//        this.petConsultList = petConsultList;
//    }

    @Override
    public String toString() {
        return "Pet{" +
                "petId=" + petId +
                ", race='" + race + '\'' +
                ", birthdate=" + birthdate +
                ", isVaccinated=" + isVaccinated +
                ", ownerName='" + ownerName + '\'' +
                "}\n";
    }
}
