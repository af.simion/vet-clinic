package com.sda.petclinic.menu;

import com.sda.petclinic.HibernateUtils;
import com.sda.petclinic.dao.ConsultDao;
import com.sda.petclinic.dao.PetDao;
import com.sda.petclinic.dao.VeterinarianDao;
import org.hibernate.Session;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

    Session session = HibernateUtils.getSessionFactory().openSession();
    session.close();

    Menu menu = new Menu();
    int option = 1;
        while (option != 0) {
            menu.displayMenu();
            option = menu.chooseOption();
        }
    }
}
