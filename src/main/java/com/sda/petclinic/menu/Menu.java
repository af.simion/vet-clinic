package com.sda.petclinic.menu;

import com.sda.petclinic.dao.ConsultDao;
import com.sda.petclinic.dao.PetDao;
import com.sda.petclinic.dao.VeterinarianDao;
import com.sda.petclinic.model.Consult;
import com.sda.petclinic.model.Pet;
import com.sda.petclinic.model.Veterinarian;

import java.sql.Date;
import java.util.List;
import java.util.Scanner;

public class Menu {

    private VeterinarianDao vetDao = new VeterinarianDao();
    private PetDao petDao = new PetDao();
    private ConsultDao consultDao = new ConsultDao();

    private Scanner in = new Scanner(System.in);

    public void displayMenu() {
        System.out.println("0. Exit\n" +
                "1. Add vet\n" +
                "2. Update vet\n" +
                "3. Delete vet\n" +
                "4. Show vets\n" +
                "5. Add pet\n" +
                "6. Update vet\n" +
                "7. Delete pet\n" +
                "8. Show pets\n" +
                "9. Add consult\n" +
                "10. Update consult\n" +
                "11. Show consults");
    }

    public int chooseOption() {
        System.out.println("Choose an option: ");
        int option = in.nextInt();
        switch (option) {
            case 0:
                System.out.println("Program is closing...");
                return option;
            case 1:
                addVet();
            case 2:
                updateVet();
                break;
            case 3:
                deleteVet();
                break;
            case 4:
                showVets();
                break;
            case 5:
                addPet();
            case 6:
                updatePet();
                break;
            case 7:
                deletePet();
                break;
            case 8:
                showPets();
                break;
            case 9:
                addConsult();
            case 10:
                updateConsult();
                break;
            case 11:
                showConsults();
            break;
            default:
                System.out.println("Option is invalid");
                this.chooseOption();
                break;
        }
        return 1;
    }

    private void addVet() {
        System.out.println("You chose to add a vet");
        System.out.println("First Name: ");
        in.nextLine();
        String firstName = in.nextLine();
        System.out.println("Last Name: ");
        in.nextLine();
        String lastName = in.nextLine();
        System.out.println("Speciality: ");
        String speciality = in.nextLine();
        System.out.println("Address: ");
        String address = in.nextLine();
        Veterinarian vet = new Veterinarian(firstName, lastName, speciality, address);
        vetDao.add(vet);
        System.out.println("The vet was successfully added");
    }

    private void updateVet() {
        System.out.println("You chose to update an existing vet");
        in.nextLine();
        System.out.println("Alege un veterinar din lista: ");
        showVets();
        int vetToUpdate = in.nextInt();
        Veterinarian updateVet = vetDao.findById(vetToUpdate);
        System.out.println("Ce doriti sa schimbati: \n" +
                "1. Adresa veterinarului \n" +
                "2. Specializarea veterinarului \n");
        int option = in.nextInt();
        do {
            switch (option) {
                case 1:
                    System.out.println("Adress: ");
                    in.nextLine();
                    String address = in.nextLine();
                    updateVet.setAddress(address);
                    vetDao.update(updateVet);
                    break;
                case 2:
                    System.out.println("Speciality: ");
                    in.nextLine();
                    String speciality = in.nextLine();
                    updateVet.setSpeciality(speciality);
                    vetDao.update(updateVet);
                    break;
                default:
                    System.out.println("Option is invalid");
            }
            option = in.nextInt();
        } while (option == 1 || option == 2);
        System.out.println("The vet was successfully updated");
    }

    private void deleteVet() {
        System.out.println("You chose to delete a vet");
        in.nextLine();
        System.out.println("Alege un veterinar din lista: ");
        showVets();
        int vetToDelete = in.nextInt();
        Veterinarian deleteVet = vetDao.findById(vetToDelete);
        vetDao.delete(deleteVet);
        System.out.println("The vet was successfully deleted");
    }

    private void showVets () {
        System.out.println("These are the vets from database:");
        List <Veterinarian> vetList = vetDao.getAllVeterinarians();
        System.out.println(vetList);
    }

    public void addPet(){
        System.out.println("You chose to add a pet");
        in.nextLine();
        System.out.println("Race: ");
        String race = in.nextLine();
        System.out.println("Birth date, between quotes: ");
        Date birthDate = Date.valueOf(in.next());
        System.out.println("Is the dog vaccinated: ");
        boolean isVaccinated = in.nextBoolean();
        System.out.println("Owner name: ");
        in.nextLine();
        String ownerName = in.nextLine();
        Pet pet = new Pet(race, birthDate, isVaccinated, ownerName);
        petDao.add(pet);
        System.out.println("The pet was successfully added.");
    }

    private void updatePet() {
        System.out.println("You chose to update an existing pet");
        in.nextLine();
        System.out.println("Alege un animal din lista dupa id: ");
        showPets();
        int petToUpdate = in.nextInt();
        Pet updatePet = petDao.findById(petToUpdate);
        System.out.println("Ce doriti sa modificati: \n" +
                "1. Denumirea rasei animalului \n" +
                "2. Este vaccinat \n" +
                "3. Data de nastere a animalului \n" +
                "4. Denumirea proprietarului \n");
        int option = in.nextInt();
        do{
            switch (option) {
                case 1:
                    System.out.println("Race: ");
                    in.nextLine();
                    String race = in.nextLine();
                    updatePet.setRace(race);
                    petDao.update(updatePet);
                    break;
                case 2:
                    System.out.println("Is Vaccinated: ");
                    in.nextLine();
                    boolean isVaccinated = in.nextBoolean();
                    updatePet.setIsvaccinated(isVaccinated);
                    petDao.update(updatePet);
                    break;
                case 3:
                    System.out.println("Birthdate: ");
                    in.nextLine();
                    updatePet.setBirthdate(Date.valueOf(in.next()));
                    petDao.update(updatePet);
                    break;
                case 4:
                    System.out.println("Owner name: ");
                    in.nextLine();
                    String ownerName = in.nextLine();
                    updatePet.setOwnerName(ownerName);
                    petDao.update(updatePet);
                    break;
                default:
                    System.out.println("Option is invalid");
                    break;
            }
            option = in.nextInt();
        } while (option >=1 && option <= 4);
        System.out.println("The pet was successfully updated");
    }

    private void deletePet() {
        System.out.println("You chose to delete a pet");
        in.nextLine();
        System.out.println("Lista de veterinari este: ");
        showPets();
        System.out.println("Alege veterinarul dorit din lista pentru a fi sters dupa Id: ");
        int vetToDelete = in.nextInt();
        Veterinarian deleteVet = vetDao.findById(vetToDelete);
        vetDao.delete(deleteVet);
        System.out.println("The vet was successfully deleted");
    }

    private void showPets () {
        System.out.println("The pets from database are: ");
        List<Pet> petList = petDao.getAllPets();
        System.out.println(petList);
    }

    private void addConsult() {
        System.out.println("You chose to add a consult");
        System.out.println("Introduceti data consultului: ");
        in.nextLine();
        Date consultDate = Date.valueOf(in.next());
        System.out.println("Descriere: ");
        in.nextLine();
        String description = in.nextLine();
        System.out.println("Alege un veterinar din lista: ");
        showVets();
        Veterinarian veterinarian = vetDao.findById(in.nextInt());
        System.out.println("Alege un animal din lista: ");
        showPets();
        Pet pet = petDao.findById(in.nextInt());
        Consult consult = new Consult(consultDate, description, veterinarian, pet);
        consultDao.addConsult(consult);
        System.out.println("The consult was successfully added");
    }

    private void updateConsult() {
        System.out.println("You chose to update an existing consult");
        in.nextLine();
        System.out.println("Alege un consult din lista: ");
        showConsults();
        int consultToUpdate = in.nextInt();
        Consult updateConsult = consultDao.findById(consultToUpdate);
        System.out.println("Ce doriti sa schimbati: \n" +
                "1. Veterinarul animalului \n" +
                "2. Descrierea consultului \n" +
                "3. Data consultului \n");
        int option = in.nextInt();
        do {
        switch (option) {
            case 1:
                System.out.println("Alege un veterinar din lista: ");
                showVets();
                in.nextLine();
                Veterinarian veterinarian = vetDao.findById(in.nextInt());
                vetDao.update(veterinarian);
                consultDao.updateConsult(updateConsult);
                break;
            case 2:
                System.out.println("Description: ");
                in.nextLine();
                String description = in.nextLine();
                updateConsult.setDescription(description);
                consultDao.updateConsult(updateConsult);
                break;
            case 3:
                System.out.println("Date: ");
                in.nextLine();
                String date = in.nextLine();
                updateConsult.setDate(Date.valueOf(date));
                consultDao.updateConsult(updateConsult);
                break;
            default:
                System.out.println("Option is invalid");
        }
        option = in.nextInt();
    } while (option >= 1 && option <= 3);
        System.out.println("The consult was successfully updated");
}

    private void showConsults () {
         System.out.println("Here are the consults from database: ");
         List<Consult> consultList = consultDao.getAllConsults();
         System.out.println(consultList);
    }
}

