# Vet Clinic Management System

This JAVA project is meant to create an application to manage 
a veterinarian clinic

# Requirements

 - JAVA 8
 - Maven
 - Database version (MySQL 8.0)
 
# Project Structure

The project is structured as follows:
 - 3 existing folders -> model - contains object classes
                      -> dao - contains methods classes
                      -> menu - contains the Menu and Main classes
 - HibernateUtils Class with the connection to the database requirements
 - pom.xml with Maven dependencies
 
# How to run

Users have to run Main Class, choosing step by step one 
of the options written in the menu

# Functionalities

The application allows the user to:
 - view all the recordings from database
 - add, update or delete recordings 
 
# License & Copyright

Licensed under the MIT License [MIT License](License)
